import numpy as np
import torch
import torch.nn as nn


def tile(a, dim, n_tile):
    order_index=torch.repeat_interleave(torch.arange(n_tile).cuda(), repeats=n_tile, dim=0)
    return torch.index_select(a, dim, order_index)


def pairwise_distances(x, k):
    x_extended = tile(x, 0, x.shape[0])
    y_extended = torch.cat(x.shape[0] * [x])
    dist = torch.sum((x_extended - y_extended) ** 2, dim=1)
    dist = dist.reshape(x.size()[0], x.size()[0], -1)
    knn = dist.topk(k, largest=False, dim=1)
    knn_matrix=torch.index_select(x, 0,knn.indices.view(-1))
    knn_matrix_reshaped=knn_matrix.reshape(x.size()[0], k, -1)
    centers = torch.sum(knn_matrix_reshaped, dim=1) / knn_matrix.shape[1]
    centers_extended=torch.repeat_interleave(centers, repeats=k, dim=0).reshape(knn_matrix_reshaped.shape[0],k,knn_matrix_reshaped.shape[2])
    std_normalized = torch.sqrt(torch.sum(torch.sum(((knn_matrix_reshaped - centers_extended) ** 2),dim=1), dim=1) / knn_matrix_reshaped.shape[1])
    return std_normalized


def pyKnn(data, test, k):
    dist=torch.sum((data-test)**2,dim=1)
    knn = dist.topk(k, largest=False, dim=0)
    knn_matrix = torch.index_select(data, 0, knn.indices.view(-1))
    centers = torch.sum(knn_matrix, dim=0) / knn_matrix.shape[0]
    std_normalize = torch.sqrt(torch.sum(torch.sum(((knn_matrix - centers) ** 2),dim=0)) / knn_matrix.shape[0])
    std_normalize_repeated=std_normalize.repeat(len(data))
    return std_normalize_repeated


class s2r_loss(nn.Module):
    def __init__(self, dim_hidden, num_classes,args, lambda_c):
        super(s2r_loss, self).__init__()
        self.dim_hidden = dim_hidden
        self.num_classes = num_classes
        self.lambda_c = lambda_c
        self.centers = nn.Parameter(torch.randn(self.num_classes, self.dim_hidden, dtype=torch.double).cuda(),requires_grad=True)
        self.K_normalizer=nn.Parameter(torch.randn(self.num_classes, 1, dtype=torch.double).cuda(),requires_grad=True)
        self.knn = args.knn

    def forward(self, hidden, y, graft, epoch, randomness_flag):
        K_normalizer_cosine = (40 * torch.sigmoid(self.K_normalizer) + 5)
        batch_size = hidden.size()[0]
        class_instances = [hidden[(y == i)] for i in range(0, self.num_classes)]
        dynamic_knn=[np.random.randint(2, class_instances[i].shape[0] - 1) if class_instances[i].shape[0] < self.knn else self.knn for i in range(0, len(class_instances))]
        inter, intra, tmp_intra = 0., 0., 0.
        inflection_point_centers=[pyKnn(class_instances[i], self.centers[i], dynamic_knn[i]) for i in range(0, len(class_instances))]
        for i in range(0, len(class_instances)):
                c = (10 / (1 + torch.exp(-(K_normalizer_cosine[i].repeat(len(class_instances[i]))) * (pairwise_distances(class_instances[i], dynamic_knn[i]) - inflection_point_centers[i])))).reshape(len(class_instances[i]), 1)
                n_instances=len(class_instances[i])
                tmp_expanded_center= self.centers[i].repeat(n_instances)
                expanded_center = tmp_expanded_center.reshape(-1, self.centers.size()[1])
                a = torch.sum(((class_instances[i] - expanded_center) ** 2), dim=1) * torch.flatten(c)
                intra += torch.sum(a)

        loss = (self.lambda_c / batch_size) * intra
        return loss
