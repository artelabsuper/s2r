import datetime
import os
import os.path as osp
import random
import time

import matplotlib

from ConvNet import ConvNet
from sigma2_R import s2r_loss
from utils import AverageMeter, compute_intravariance

matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import torch
import torch.nn as nn
from torch.optim import lr_scheduler
from center_loss import CenterLoss

# def save_images(data, labels, index_centroids, class_string):
#     index_centroids = np.concatenate(index_centroids)
#     number_clusters = len(set(index_centroids))
#     for i in range(number_clusters):
#         if not osp.exists('Clusters_gpu' + args.gpu + '_/Cluster_' + str(i)):
#             os.makedirs('Clusters_gpu' + args.gpu + '_/Cluster_' + str(i))
#     for index, (img, label, idx_cent, cl_str) in enumerate(zip(data, labels, index_centroids, class_string)):
#         plt.imsave('Clusters_gpu' + args.gpu + '_/Cluster_' + str(idx_cent) + '/img_' + str(cl_str) + "_" + str(
#             index) + '_label_' + str(label), img.transpose(1, 2, 0),
#                    format='png')  # astype(np.uint8) remember your data
#         # torchvision.utils.save_image(torch.from_numpy(img), "Clusters/Cluster_"+str(idx_cent)+'/img_'+str(index)+'_label_'+str(label))


def main(args, trainloader, testloader, num_classes, file_output):
        model = ConvNet(num_classes=num_classes)
        dim_features=model.fc1.out_features
        #model = LeNet(num_classes=num_classes)
        #model = ResNet18(num_classes=num_classes)
        #dim_features = model.linear.in_features

        #dim_features = model.fc2.out_features

        # model= Classifier()
        # dim_features=2

        model = nn.DataParallel(model).cuda()
        criterion_xent = nn.CrossEntropyLoss()#TODO
        criterion_loss=None

        if args.loss == 'center_loss':
            print("CenterLoss set")
            print("Features dimension: ", dim_features)
            criterion_loss = CenterLoss(dim_hidden=dim_features, num_classes=num_classes, lambda_c=args.lambda0)

        if args.loss == 'sigma2_R':
            print("sigma^2_R loss set")
            print("Features dimension: ", dim_features)
            criterion_loss = s2r_loss(dim_hidden=dim_features, num_classes=num_classes, args=args, lambda_c=args.lambda0)

        if args.loss =='xent_loss':
            optimizer = torch.optim.Adam(model.parameters(), args.lr)
            optimizer_c=None
        else:
            optimizer = torch.optim.Adam(model.parameters(), args.lr)
            optimizer_c = torch.optim.Adam(criterion_loss.parameters(), args.lr_cent)

        scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch)
        if args.loss != 'xent_loss':
            scheduler_cent = lr_scheduler.CosineAnnealingLR(optimizer_c, args.max_epoch)

        start_time = time.time()
        for epoch in range(args.max_epoch):
            lr=scheduler.get_lr()[0]
            if args.loss != 'xent_loss':
                lr_cent = scheduler_cent.get_lr()[0]
                print("LR: ", lr, "LR cent: ", lr_cent)
            if args.loss == 'sigma2_R':
                print("K_normalizer: ", criterion_loss.K_normalizer.data.cpu().numpy().transpose(), file=file_output)
            train(trainloader, model, criterion_xent, criterion_loss,
                  optimizer,optimizer_c, num_classes, epoch, args, file_output)
            print("==> Epoch {}/{}".format(epoch + 1, args.max_epoch), file=file_output)

            #if epoch % 10 == 0:
            scheduler.step()
            if args.loss != 'xent_loss':
                scheduler_cent.step()

            if args.eval_freq > 0 and (epoch + 1) % args.eval_freq == 0:
                print("==> Test", file=file_output)
                acc, err = test(model, testloader, args, file_output,num_classes)
                print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err), file=file_output)

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))
        return True


def train(trainloader, model, criterion_xent, criterion_loss,
          optimizer,optimizer_c, num_classes, epoch, args, file_output):
    model.train()
    xent_losses = AverageMeter()
    pot_losses = AverageMeter()
    center_losses = AverageMeter()
    all_features, all_labels, all_paths = [], [], []
    randomness_flag= bool(random.getrandbits(1))
    for batch_idx, (data, labels) in enumerate(trainloader):
            data, labels = data.cuda(), labels.cuda()
            outputs, features = model(data)
            #x_max, x_min = torch.max(features), torch.min(features)
            loss_xent = criterion_xent(outputs, labels)
            if args.loss == 'sigma2_R':
                sigma2_R = criterion_loss(features, labels, args.graft, epoch, randomness_flag)
                loss = sigma2_R + loss_xent
            if args.loss == 'xent_loss':
                loss = loss_xent
            elif args.loss == 'center_loss':
                center_loss = criterion_loss(features, labels)
                loss = loss_xent + center_loss

            optimizer.zero_grad()

            if args.loss != 'xent_loss':
                optimizer_c.zero_grad()

            #TODO to detach parameter from learning step (not computed during gradient descendent)
            # if args.graft <= epoch:
            #     criterion_loss.centers.detach_()


            loss.backward()
            optimizer.step()
            if args.loss != 'xent_loss':
                optimizer_c.step()

            xent_losses.update(loss_xent.item(), labels.size(0))
            all_features.append(features.data.cpu().numpy())
            all_labels.append(labels.data.cpu().numpy())
            #all_paths.append(trainloader.dataset.samples[batch_idx*args.batch_size:(batch_idx+1)*args.batch_size]) #to get paths images
            if args.loss == 'sigma2_R':
                pot_losses.update(sigma2_R.item(), labels.size(0))
                if (batch_idx + 1) % 80 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f}) PotLoss {:.6f} ({:.6f})".format(batch_idx + 1,
                                                                                                   xent_losses.val,
                                                                                                   xent_losses.avg,
                                                                                                   pot_losses.val,
                                                                                                   pot_losses.avg),
                          file=file_output)

            if args.loss == 'center_loss':
                center_losses.update(center_loss.item(), labels.size(0))
                if (batch_idx + 1) % 80 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f}) CenterLoss {:.6f} ({:.6f})".format(batch_idx + 1,
                                                                                                      xent_losses.val,
                                                                                                      xent_losses.avg,
                                                                                                      center_losses.val,
                                                                                                      center_losses.avg),
                          file=file_output)

            if args.loss == 'xent_loss':
                if (batch_idx + 1) % 80 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1,xent_losses.val,xent_losses.avg),file=file_output)

    all_features = np.concatenate(all_features, 0)
    all_labels = np.concatenate(all_labels, 0)
    #all_paths = np.concatenate(all_paths, 0)
    if (epoch + 1) % 1 == 0:
        compute_intravariance("Train", all_features, all_labels, num_classes, file_output)
        plot_features(all_features, all_labels, num_classes, epoch, args, all_paths,criterion_loss)


def test(model, testloader, args, file_output, num_classes):
    model.eval()
    all_features, all_labels=[],[]
    correct, total = 0, 0
    confusion_matrix = np.zeros((2, 2), dtype=int)
    with torch.no_grad():
        for idx, (data, labels) in enumerate(testloader):
                data, labels = data.cuda(), labels.cuda()
                outputs, features = model(data)

                all_features.append(features.data.cpu().numpy())
                all_labels.append(labels.data.cpu().numpy())

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().float().item()

                #for t, p in zip(predicted, labels):
                    #confusion_matrix[int(t), int(p)] += 1



    all_features = np.concatenate(all_features, 0)
    all_labels = np.concatenate(all_labels, 0)
    compute_intravariance("Test", all_features, all_labels, num_classes, file_output)
    #print("Precision 0: ",confusion_matrix[0][0]/np.sum(confusion_matrix[0]), file=file_output)
    #print("Precision 1: ", confusion_matrix[1][1] / np.sum(confusion_matrix[1]), file=file_output)
    acc = correct * 100. / total
    err = 100. - acc
    return acc, err


# def plot_accuracy_epochs(acc_list):
#     fig, ax = plt.subplots()
#     ax.plot([int(i) for i in range(args.max_epoch)], acc_list)
#
#     ax.set(xlabel='Epochs', ylabel='Accuracy',
#            title='Accuracy over epochs')
#     ax.grid()
#     dirname = osp.join(args.save_dir)
#     fig.savefig(dirname + "/accuracy_over_epochs_deep_" + args.loss + "_" + str(args.deep_level) + "_" + str(
#         args.batch_size) + " lr:" + str(args.lr) + " lr_cent" + str(args.lr_cent) + ".png")


def plot_features(features, labels, num_classes, epoch, args, all_paths, criterion_loss):
    #centers=criterion_loss.centers.data.cpu().numpy()
    all_colors = num_classes
    colors = .6 * np.random.rand(all_colors, 3) + 0.4
    #colors=['R','G','B']
    plt.tick_params(labelsize=14)
    #splitted=[i[0].split('_') for i in all_paths]
    #class_color=[i[1][-1] for i in splitted]
    #idx_instance = [i[2] for i in splitted]
    #noise_color = [i[3] for i in splitted]

    # tsne = TSNE(n_components=2, init='pca', n_iter=250)
    # tSNE_result = tsne.fit_transform(features)
    # tSNE_label = labels
    # print(tSNE_result)
    # exit()
    for label_idx in range(num_classes):
        plt.scatter(
            features[labels == label_idx, 0],
            features[labels == label_idx, 1],
            facecolor=colors[label_idx],
            #marker=noise_marker,
            s=4,
        )

    # for feat,l,cl_color,noise in zip(features,labels,class_color,noise_color):
    #     if noise=='N':
    #         plt.plot(feat[0],feat[1],c=colors[l], marker='o', markersize=1)
    #     else:
    #         plt.plot(feat[0], feat[1], c=colors[l], marker='x', markersize=10)

    #plt.scatter(centers[:,0],centers[:,1], marker='x', s=5, facecolor='black')

    # plt.legend(loc='upper right')
    dirname = osp.join(args.save_dir, 'plot_' + args.path.split('/')[-2] + '_'+args.loss)
    if not osp.exists(dirname):
        os.mkdir(dirname)
    save_name = osp.join(dirname, 'epoch_' + str(epoch + 1) + '.png')
    plt.savefig(save_name, bbox_inches='tight', dpi=400)
    plt.close()


if __name__ == '__main__':
    main()
