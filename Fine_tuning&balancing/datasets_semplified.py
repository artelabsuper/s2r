from torchvision import datasets
from torchvision.datasets import ImageFolder
#from tqdm import tqdm
from torch.utils.data import DataLoader, DistributedSampler

from extra_sampler import DistributedSamplerWrapper, BalanceBatchSampler
from utils import load_imagenet_annotations
from torch.utils.data.dataset import Dataset
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler, WeightedRandomSampler
import PIL.Image
import torch
import albumentations as albu
import numpy
import torchvision
from PIL import Image
#from scipy.ndimage import imread
from torch.utils.data import DataLoader, SequentialSampler
from torchvision.datasets.utils import download_url, check_integrity
#from class_select import DatasetMaker, get_class_i
#from Albumentations_loader import AlbuDataLoader
import new_ImageFolder
from utils import PartialBalancedBatchSampler


# def get_training_augmentation():
#     train_transform = [
#      #albu.IAAAdditiveGaussianNoise(p=0.5),
#      #albu.IAAAdditiveGaussianNoise(p=0.5),
#      #albu.ShiftScaleRotate(p=0.5),
#      #albu.IAAPerspective(p=0.5),
#     albu.OneOf([
#         #albu.VerticalFlip(),
#         albu.HorizontalFlip(),
#     ], p=0.5),
#
#      albu.Rotate(p=0.5),
#      albu.RandomResizedCrop(32,32, scale=(0.7, 1.0),p=0.5),
#      #albu.RandomBrightnessContrast(p=0.3),
#      #albu.CLAHE(p=0.3),
#      #albu.Blur(p=0.3)
#      #albu.ChannelShuffle(p=0.5),
#      #albu.RandomGamma(p=0.5),
#      #albu.RandomGridShuffle(grid=(3, 3), p=0.5),
#      #albu.RandomRotate90(p=0.5),
#     ]
#     return albu.Compose(train_transform,p=0.5)
#
#
#
# class MyDataset(object):
#     def __init__(self, batch_size, use_gpu, num_workers, percentage_used, path):
#         trainloader=[]
#         train_labels=[]
#         testloader=[]
#         test_labels=[]
#         self.batch_size = batch_size
#         self.percentage_used = percentage_used
#         self.path=path
#
#         transform = [torchvision.transforms.RandomRotation(30), torchvision.transforms.RandomHorizontalFlip(),
#                      torchvision.transforms.RandomVerticalFlip(),
#                      #torchvision.transforms.ColorJitter(brightness=(0, 50), contrast=(0, 30), saturation=(0, 40),hue=(-0.5, 0.5)),
#                      torchvision.transforms.RandomResizedCrop(size=(28,28), scale=(0.9, 1.0))]
#
#         train_transforms = torchvision.transforms.Compose([
#             #torchvision.transforms.Resize(size=(32, 32)),
#             #torchvision.transforms.RandomApply(random_pick(transform), p=0.5),
#             torchvision.transforms.ToTensor(),
#         ])
#
#         test_transforms = torchvision.transforms.Compose([
#             #torchvision.transforms.Resize(size=(32, 32)),
#             torchvision.transforms.ToTensor(),
#         ])
#
#
#         train_dataset = datasets.ImageFolder(path+'/train', transform=train_transforms)
#         train_idx=[i for i in range(len(train_dataset.samples))]
#         train_loader = torch.utils.data.DataLoader(train_dataset, sampler=BalancedBatchSampler(train_dataset, train_idx), batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True) #for each batch we have batch-size/class instances.
#
#         test_dataset = datasets.ImageFolder(path + '/test', transform=test_transforms)
#         test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)
#
#         self.trainloader = train_loader
#         self.testloader = test_loader
#         self.num_classes = len(train_dataset.classes)
#         print("Full Dataset size: ", len(train_dataset.samples), " Dataset percentage used: ", percentage_used * 100, "% ","Classes: ", self.num_classes, "Test loader set: ", len(test_dataset.samples))
#
#
# class MyDataset_cross_val(object):
#     def __init__(self, batch_size, path, balanced_classes):
#         self.batch_size = batch_size
#         self.path=path
#
#         train_transforms = torchvision.transforms.Compose([
#             torchvision.transforms.Resize((32,32)),
#             ToNumpy(),
#             AlbumentationToTorchvision(get_training_augmentation()),
#             torchvision.transforms.ToTensor()
#         ])
#
#         # test_transforms = torchvision.transforms.Compose([
#         #     torchvision.transforms.ToTensor(),
#         # ])
#
#
#
#
#         train_dataset = ImageFolder(path, transform=train_transforms)
#         num_train = len(train_dataset)
#         indices = list(range(num_train))
#         split = int(np.floor(0.2 * num_train))
#
#         np.random.seed(np.random.randint(0,1000))
#         np.random.shuffle(indices)
#         train_idx, valid_idx = indices[split:], indices[:split]
#         train_sampler = SubsetRandomSampler(train_idx)
#         test_sampler = SubsetRandomSampler(valid_idx)
#
#
#
#         train_loader = torch.utils.data.DataLoader(train_dataset,
#                                                    sampler=Partial_BalancedBatchSampler(train_dataset, train_idx,
#                                                                                         balanced_classes=balanced_classes,
#                                                                                         num_classes=len(
#                                                                                             train_dataset.classes)),
#                                                    batch_size=batch_size, shuffle=False, num_workers=16,
#                                                    pin_memory=True)
#
#         test_loader = torch.utils.data.DataLoader(train_dataset, sampler=test_sampler, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)
#
#
#
#         self.trainloader = train_loader
#         self.testloader = test_loader
#         self.num_classes = len(train_dataset.classes)
#         print("Full Dataset size: ", len(train_dataset), "Classes: ", self.num_classes, "Test loader set: ",len(test_loader)*batch_size)
#
#
# class eggsmentations(object):
#     def __init__(self, batch_size, path, balanced_classes):
#         self.batch_size = batch_size
#         self.path=path
#         self.balanced_classes=balanced_classes
#
#         normalize=torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
#                                      std=[0.229, 0.224, 0.225])
#
#         train_transforms = torchvision.transforms.Compose([
#             #torchvision.transforms.Resize((32, 32)),
#             ToNumpy(),
#             AlbumentationToTorchvision(get_training_augmentation()),
#             torchvision.transforms.ToTensor(),
#             #normalize
#         ])
#
#         test_transforms = torchvision.transforms.Compose([
#             #torchvision.transforms.Resize((32,32)),
#             torchvision.transforms.ToTensor(),
#             #normalize
#         ])
#
#
#         train_dataset = datasets.ImageFolder(self.path + '/train', transform=train_transforms)
#
#         train_idx = [i for i in range(len(train_dataset))]
#
#         train_loader = torch.utils.data.DataLoader(train_dataset,sampler=Partial_BalancedBatchSampler(train_dataset, train_idx, balanced_classes=balanced_classes, num_classes=len(train_dataset.classes)),batch_size=batch_size, shuffle=False, num_workers=16,pin_memory=True)
#         #train_loader = torch.utils.data.DataLoader(train_dataset,batch_size=batch_size, shuffle=False, num_workers=16,pin_memory=True)
#
#         test_dataset = datasets.ImageFolder(self.path + '/test', transform=test_transforms)
#         test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)
#
#         self.trainloader = train_loader
#         self.testloader = test_loader
#         self.num_classes = len(train_dataset.classes)
#         print("Full Dataset size: ", len(train_dataset),"Classes: ", self.num_classes, "Test loader set: ", len(test_dataset.samples))
#         # for data,label in train_loader:
#         #     class_instances = [data[(label == i)] for i in range(0, self.num_classes)]
#         #     print([len(i) for i in class_instances])
#         # exit()


class MyDataset_plus_annotation(object):
    def __init__(self, batch_size, path, balanced_classes):
        self.batch_size = batch_size
        self.path=path

        #Imagenet preprocessing
        # train_transforms = torchvision.transforms.Compose([
        #     torchvision.transforms.RandomResizedCrop(224),
        #     torchvision.transforms.RandomHorizontalFlip(),
        #     torchvision.transforms.ToTensor(),
        #     torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        # ])
        #
        # test_transforms = torchvision.transforms.Compose([
        #     torchvision.transforms.Resize(256),
        #     torchvision.transforms.CenterCrop(224),
        #     torchvision.transforms.ToTensor(),
        #     torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        # ])
        #
        # train_dataset = datasets.ImageFolder(self.path+'/train', transform=train_transforms)
        # test_dataset = datasets.ImageFolder(self.path+'/test', transform=test_transforms)

        #train_idx = [i for i in range(len(train_dataset))]

        # train_loader = torch.utils.data.DataLoader(train_dataset,sampler=Partial_BalancedBatchSampler(train_dataset, train_idx,
        #                                                                                 balanced_classes=balanced_classes,
        #                                                                                 num_classes=len(
        #                                                                                     train_dataset.classes)),
        #                                            batch_size=batch_size, shuffle=False, num_workers=8,
        #                                            pin_memory=True)
        self.dataset_name = path.split('/')[-1]
        print("Dataset selected: ", self.dataset_name)

        if self.dataset_name == 'CUB_200_2011':
            print(self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.RandomResizedCrop(448),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize(int(448 / 0.875)),
                torchvision.transforms.CenterCrop(448),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])
        elif self.dataset_name == 'cifar100' or self.dataset_name == 'cifar10':
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.RandomCrop(32, padding=4),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=[0.507, 0.487, 0.441], std=[0.267, 0.256, 0.276])
            ])

            # Normalize test set same as training set without augmentation
            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=[0.507, 0.487, 0.441], std=[0.267, 0.256, 0.276])
            ])
        elif self.dataset_name == 'fgvc_dataset':
            # rate=0.875
            print("Aircraft ", self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((448, 448), Image.BILINEAR),
                # torchvision.transforms.RandomCrop((448,448)),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ColorJitter(brightness=0.2, contrast=0.2),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((448, 448), Image.BILINEAR),
                # torchvision.transforms.CenterCrop(448),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])
        elif self.dataset_name == 'dogs_classification':
            print(self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize([512, 512]),
                torchvision.RandomCrop([448, 448]),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize([512, 512]),
                torchvision.transforms.CenterCrop(448),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])
        elif self.dataset_name == 'Imagenet-ILSVRC2012':
            print("Dataset loading ", self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.RandomResizedCrop(224),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize(256),
                torchvision.transforms.CenterCrop(224),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])


        train_dataset = datasets.ImageFolder(self.path + '/train', transform=train_transforms)
        b_balanced = PartialBalancedBatchSampler(train_dataset,balanced_classes=balanced_classes,num_classes=len(train_dataset.classes))
        train_loader = torch.utils.data.DataLoader(train_dataset, sampler=b_balanced,batch_size=batch_size, shuffle=False, num_workers=1, pin_memory=True)

        if self.dataset_name =='Imagenet-ILSVRC2012':
            test_dataset = datasets.ImageFolder(self.path + '/val', transform=test_transforms)
        else:
            test_dataset = datasets.ImageFolder(self.path + '/test', transform=test_transforms)

        test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=1,
                                                  pin_memory=True)


        #b_balanced=BalanceBatchSampler(train_dataset.targets, p=len(train_dataset.classes), k=50)
        # b_balanced = torch.utils.data.distributed.DistributedSampler(b_balanced,shuffle=False)

        # train_loader = torch.utils.data.DataLoader(train_dataset, sampler=b_balanced, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)
        #
        #
        # test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=16,
        #                                           pin_memory=True)

        self.trainloader = train_loader
        self.testloader = test_loader
        self.num_classes = len(train_dataset.classes)
        print("Full Dataset size: ", len(train_dataset), "Classes: ", self.num_classes, "Test loader set: ",
              len(test_dataset.samples))
        # for data,label in train_loader:
        #     class_instances = [data[(label == i)] for i in range(0, self.num_classes)]
        #     print(sum([len(i) for i in class_instances]))
        # exit()



class AlbumentationToTorchvision:
    def __init__(self, compose):
        self.compose = compose

    def __call__(self, x):
        data = {"image": x}
        return self.compose(**data)['image']


class ToNumpy:
    def __call__(self, x):
        return numpy.array(x)




__factory = {
    # 'MyDataset': MyDataset,
    # 'MyDataset_cross_val': MyDataset_cross_val,
    # 'eggsmentations': eggsmentations,
    'MyDataset_plus_annotation': MyDataset_plus_annotation
}


def create(name, batch_size, path, balanced_classes):
    if name not in __factory.keys():
        raise KeyError("Unknown dataset: {}".format(name))
    return __factory[name](batch_size, path, balanced_classes)
