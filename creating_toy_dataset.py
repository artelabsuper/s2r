import os

import numpy as np
from PIL import Image

import utils

classes=3
instances=4000
path="/home/nataraja/Pictures/potential_loss/dataset_toy/"
utils.mkdir_if_missing(path)
list_dir=os.listdir(path)
#list_dir.remove('intravariance')

noise_images=int(instances*30/100)

for i in range(classes):
    utils.mkdir_if_missing(path+'/'+str(i))
    for j in range(instances):
        if j < noise_images:
            random_color_channel = np.random.randint(50, 255, 1)
            noise_color_channel=np.random.randint(0,50,1)
            if i == 0:
                random_choose = np.random.choice([1,2])
                if random_choose==1:
                    image = Image.new("RGB", (32, 32), color=(random_color_channel, noise_color_channel, 0))
                if random_choose==2:
                    image = Image.new("RGB", (32, 32), color=(random_color_channel, 0, noise_color_channel))
                image.save(path + '/' + str(i) + '/' + str(i)+'_'+str(j)+'_G_' + ".png")
            if i == 1:
                random_choose = np.random.choice([0,2])
                if random_choose == 0:
                    image = Image.new("RGB", (32, 32), color=(noise_color_channel, random_color_channel, 0))
                if random_choose == 2:
                    image = Image.new("RGB", (32, 32), color=(0, random_color_channel, noise_images))
                image.save(path + '/' + str(i) + '/' + str(i)+'_'+str(j)+'_R_' + ".png")
            if i == 2:
                random_choose = np.random.choice([0,1])
                if random_choose==0:
                    image = Image.new("RGB", (32, 32), color=(noise_color_channel, 0, random_color_channel))
                if random_choose==1:
                    image = Image.new("RGB", (32, 32), color=(0, noise_color_channel, random_color_channel))
                image.save(path + '/' + str(i) + '/' + str(i)+'_'+str(j)+'_R_' + ".png")

        else:
            random_color_channel=np.random.randint(50,255,1)
            if i==0:
                image = Image.new("RGB", (32, 32),color = (random_color_channel, 0, 0))
                image.save(path + '/' + str(i) + '/' + str(i)+'_'+str(j)+'_N_' + ".png")
            if i==1:
                image = Image.new("RGB", (32, 32),color = (0, random_color_channel, 0))
                image.save(path + '/' + str(i) + '/' + str(i) + '_' + str(j) + '_N_' + ".png")
            if i == 2:
                image = Image.new("RGB", (32, 32), color=(0, 0, random_color_channel))
                image.save(path + '/' + str(i) + '/' + str(i) + '_' + str(j) + '_N_' + ".png")

        #im = Image.fromarray(image.astype('uint8'))
        image.close()