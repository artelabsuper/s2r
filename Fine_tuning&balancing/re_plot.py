import re
import os
import sys

from matplotlib import pyplot as plt
import numpy as np
from matplotlib.pyplot import show, subplots, legend, close
import utils


def plot_bar(data, title):
    fig, ax = subplots()
    domain=data.shape[0]
    for idx, i in enumerate(data.T):
        ax.plot(i , ls='-', label='Intra '+str(idx),fillstyle='none')

    ax.set(xlabel='Domain', ylabel='Range',
           title=title)
    #xticks(domain)
    ax.grid()
    #legend(loc='upper right')
    fig.savefig(path_bar+title+'.png', dpi=200)
    plt.close(fig)
    #show(bbox_inches='tight', dpi=400)



def plot_accuracy_epochs():
    fig, ax = plt.subplots()
    ax.tick_params(axis='both', which='major', labelsize=14)
    for index, i in enumerate(acc_list):
        if i[1]=='xent':
            c='red'
            ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c)
        elif i[1]=='center':
            c='blue'
            ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c)
        #elif i[1] == 'MCG_2-9 600':
        #    c='green'
        #    ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c)
        elif i[1] == 'MCG_2-9 new':
             c='orange'
             ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c, zorder=1)
        elif i[1] == 'CGM_2-9_1000':
             c='orange'
             ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c)
        # elif i[1] == 'xent+center+maxK':
        #     c='brown'
        #     ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c)
        # elif i[1] =='w_loss+half_graft+3block':
        #     c='black'
        #     ax.plot(domain, i[2], label=i[1], linewidth=0.5, c=c)

    plt.tick_params(labelsize=14)
    plt.xlabel('Epochs', fontsize=14)
    plt.ylabel('Accuracy', fontsize=14)
    ax.grid()
    #plt.title(title)
    #plt.legend(loc='lower right')
    #show(bbox_inches='tight', dpi=400)    #plt.show(dpi=400)
    #dirname = osp.join(args.save_dir)
    fig.savefig('/home/nataraja/Desktop/comparison.png', dpi=400)
    close()


# comparison_files=['Exp:1_log_cifar10_gpu= _loss=center_loss', 'Exp:1_log_cifar10_gpu= 0_k_nor=4_knn=4_range_f=4','Exp:1_log_cifar10_gpu= 0_k_nor=12_knn=4_range_f=4','Exp:1_log_cifar10_gpu= 0_k_nor=12_knn=4_range_f=2']

marker = ['+', '.', '1','2']
lines = ["-","--","-.",":"]
#path="/home/nataraja/Desktop/cifar10/Resnet18/Experiments2/"
#path="/home/nataraja/Desktop/happy_results/cifar100/range=10/"
path="/home/nataraja/Desktop/Multiple Anchors images/cifar100/"
path_bar="/home/nataraja/Pictures/potential_loss/intravariance/101/"
utils.mkdir_if_missing(path_bar)
list_dir=os.listdir(path)
#list_dir.remove('old')
# list_dir.remove('cross_validation')
# #list_dir.remove('imagesPINS')
# list_dir.remove('risen')
# list_dir.remove('toy')
# list_dir.remove('dataset_toy')
# list_dir.remove('pins')
#list_dir.remove('readme')

acc_list=[]
regex = ('Accuracy ') + '\(%\): \d+.\d+'
regex_intra = ('Class  ')+('\d+ :  \d+ Std:  \d+.\d+')
regex_sigmoid_parameters = re.compile('K_normalizer:  \[\[+( ? ? ? ? ?-?\d+.\d+(?:[e]\ *[-|+]?\ *\d+)? ? ? ? ?\n?\]?\]?)+')


domain=[i for i in range(150,500)]
#
for i in sorted(list_dir):
    file_list = os.listdir(path + '/' + i + '/')
    for idx, j in enumerate(sorted(file_list)):
        filename = j.split('_')
        F=(open(path+i+'/'+j,'r'))
        match = re.findall(regex, F.read())
        tmp_acc=[float((j.split(': ')[1])) for j in match]
        acc_list.append([filename[4]+filename[5],i, tmp_acc[150:500]])
        F.close()


plot_accuracy_epochs()

# for i in list_dir:
#     k_list=[]
#     K_normalizer_cosine, range_f_cosine= [],[]
#     filename = i.split('_')
#     for ix,match in enumerate(re.finditer(regex_sigmoid_parameters, open(path+i,'r').read())):
#         tmp=match.group().split()
#         if '[[' in tmp:
#             tmp.remove('[[')
#         if 'K_normalizer:' in tmp:
#             tmp.remove('K_normalizer:')
#         tmp[-1]=tmp[-1].replace(']]','')
#         tmp[0] = tmp[0].replace('[[', '')
#         tmp[-1] = tmp[0].replace('\'\'', '')
#         k_list.append([float(j) for j in tmp])
#
#     k_transpose=list((map(list, zip(*k_list))))
#     plot_accuracy_epochs(filename[0]+filename[3]+filename[4]+' '+filename[7])
#
#
#
#     #for i in match:
#         #K_normalizer_cosine.append(40 * (1 / (1 + np.math.exp(-K_normalizer))) + 5)
#         #acc_list.append([filename[0]+'_0', K_normalizer_cosine[10:]])
#     # k_list_np=np.array(k_list)
#     # exit()
#     # plot_accuracy_epochs()
#     # exit()

#

#Print accuracy
dict={'cifar10':10, 'cifar100':100, 'fashion':10, '101':102, 'dataset':3} #TODO change this for PINS

for i in (sorted(list_dir)):
    file_list = os.listdir(path+'/'+i+'/')
    for idx,j in enumerate(sorted(file_list)):
        match = re.findall(regex, open(path+i+'/'+j,'r').read())
        if match:
            filename=j.split('_',3)[2]
            acc_list=[]
            #tmp_intra=[float((i.split(': ')[2])) for i in match]
            #acc_list.append(tmp_intra)
            #np.set_printoptions(threshold=sys.maxsize)
            #match_acc = re.findall(regex, open(path+folder+'/'+i,'r').read())
            tmp_acc=[float((j.split(': ')[1])) for j in match]
            #print(i, (np.mean(tmp_intra[len(tmp_intra):len(tmp_intra) - dict.get(filename)-1:-1])), tmp_acc[-1],"Max: ",np.max(tmp_acc))
            print(j,i, "Last: ",tmp_acc[-1], "--> Max: ",np.max(tmp_acc), "Avg last 5: ",np.average(tmp_acc[-1:len(tmp_acc)-5:-1]))

            #plot_bar(np.asarray(tmp_intra).reshape(-1, dict.get(filename)),i)
