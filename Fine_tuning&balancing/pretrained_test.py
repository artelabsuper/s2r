import torchvision
from torchvision import datasets
import torch
from torch.backends import cudnn
import os
import torch.nn as nn

def test(model, testloader):
    model.eval()
    all_features, all_labels=[],[]
    correct, total = 0, 0
    #confusion_matrix = np.zeros((num_classes, num_classes), dtype=int)
    with torch.no_grad():
        for idx, (data, labels) in enumerate(testloader):
                if idx >150:
                    exit()
                data, labels = data.cuda(), labels.cuda()
                outputs = model(data)
                print("Xent: ",idx, criterion_xent(outputs, labels).data.cpu().item())
                # for j in outputs:
                #     print(torch.max(j))
                #     print("Label: ", labels[0])
                #     print(j)
                #     exit()
                #all_features.append(features.data.cpu().numpy())
                #all_labels.append(labels.data.cpu().numpy())

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().float().item()

                # for t, p in zip(predicted, labels):
                #     confusion_matrix[int(t), int(p)] += 1


    #test_features = np.concatenate(all_features, 0)
    #test_labels = np.concatenate(all_labels, 0)
    #compute_intravariance("Test", torch.cat(test_features), torch.cat(test_labels), num_classes, file_output)
    #utils.compute_precision(confusion_matrix, file_output)
    # if (epoch + 1) % 99 == 0:
    #     density_plot(train_features, train_labels, test_features, test_labels, num_classes, epoch, args)
    acc = correct * 100. / total
    err = 100. - acc
    return acc, err


def ResNet18(path_model=None, pretrained=False):
    if pretrained and path_model!=None:
        from torchvision.models import resnet18
        model = resnet18(pretrained=False)
        model.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        model.fc = nn.Linear(512, 200)
        from collections import OrderedDict
        new_state_dict = OrderedDict()
        state_dict = torch.load(path_model)
        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            new_state_dict[name] = v

        # load params
        model.load_state_dict(new_state_dict)
        #model = torch.nn.DataParallel(model.cuda())
        print("Load our pretrained model")
    return model

criterion_xent = nn.CrossEntropyLoss()
path_model='/home/supreme/rlagrassa/sigma2_R/log/_CUB_200_2011_weights_model_resnet18loss_sigma2_R'
#path_model='/home/supreme/rlagrassa/sigma2_R/log/_CUB_200_2011_weights_model_resnet18loss_xent_loss'
path_test='/home/supreme/datasets/CUB_200_2011/test/'
os.environ['CUDA_VISIBLE_DEVICES'] = '0,1'
cudnn.benchmark = True
cudnn.enabled = True

pretrained_model = ResNet18(path_model=path_model, pretrained=True).cuda()

test_transforms = torchvision.transforms.Compose([
        torchvision.transforms.Resize(int(448 / 0.875)),
        torchvision.transforms.CenterCrop(448),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
    ])


test_dataset = datasets.ImageFolder('/home/supreme/datasets/CUB_200_2011/test', transform=test_transforms)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=15, shuffle=False, num_workers=1, pin_memory=True)
print("Test loader set: ", len(test_dataset.samples))
acc, err=test(pretrained_model, test_loader)
print(acc, err)