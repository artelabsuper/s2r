import torch
import torch.nn as nn


# class CenterLoss(nn.Module):
#     def __init__(self, dim_hidden, num_classes, lambda_c):
#         super(CenterLoss, self).__init__()
#         self.dim_hidden = dim_hidden
#         self.num_classes = num_classes
#         self.lambda_c = lambda_c
#         self.centers = nn.Parameter(torch.randn(num_classes, dim_hidden)).cuda()
#
#     def forward(self, hidden, y):
#         batch_size = hidden.size()[0]
#         expanded_centers = self.centers.index_select(dim=0, index=y)
#         intra_distances = hidden.dist(expanded_centers)
#         loss = (self.lambda_c / 2.0 / batch_size) * intra_distances
#         return loss


class CenterLoss(nn.Module):
    def __init__(self, dim_hidden, num_classes, lambda_c):
             super(CenterLoss, self).__init__()
             self.dim_hidden = dim_hidden
             self.num_classes = num_classes
             self.lambda_c = lambda_c
             self.centers = nn.Parameter(torch.randn(self.num_classes, self.dim_hidden).cuda())

    def forward(self, x, labels):
        """
        Args:
            x: feature matrix with shape (batch_size, feat_dim).
            labels: ground truth labels with shape (batch_size).
        """
        batch_size = x.size(0)
        distmat = torch.pow(x, 2).sum(dim=1, keepdim=True).expand(batch_size, self.num_classes) + \
                  torch.pow(self.centers, 2).sum(dim=1, keepdim=True).expand(self.num_classes, batch_size).t()
        distmat.addmm_(1, -2, x, self.centers.t())

        classes = torch.arange(self.num_classes).long()
        classes = classes.cuda()
        labels = labels.unsqueeze(1).expand(batch_size, self.num_classes)
        mask = labels.eq(classes.expand(batch_size, self.num_classes))

        dist = distmat * mask.float()
        #loss = (self.lambda_c / batch_size)* dist.sum()
        loss = (self.lambda_c / batch_size) * dist.sum()


        return loss
