import torch
import torch.nn as nn
from gmm import GaussianMixture
import numpy as np

def pairwise_distances(x, pivots):
    x_extended = torch.cat(pivots.shape[0] * [x])
    #y_extended = torch.cat((torch.cat(x.shape[0]* [a1]),torch.cat(x.shape[0] * [a2])),0)
    y_extended=tile(pivots, 0, x.shape[0])
    dist = torch.sum((x_extended - y_extended) ** 2, dim=1)
    dist = dist.reshape(pivots.shape[0], x.shape[0]).T
    top_val, top_idx = dist.topk(1, largest=False, dim=1)
    return torch.sum(top_val)



def tile(a, dim, n_tile):
    init_dim = a.size(dim)
    repeat_idx = [1] * a.dim()
    repeat_idx[dim] = n_tile
    a = a.repeat(*(repeat_idx))
    order_index = torch.LongTensor(np.concatenate([init_dim * np.arange(n_tile) + i for i in range(init_dim)])).cuda()
    return torch.index_select(a, dim, order_index)



class GMC_loss(nn.Module):
    def __init__(self, dim_hidden, num_classes, lambda_c, stretch_components_list):
        super(GMC_loss, self).__init__()
        self.dim_hidden = dim_hidden
        self.num_classes = num_classes
        self.lambda_c = lambda_c
        self.stretch_components_list= stretch_components_list
        #self.models =  [GaussianMixture(self.n_components, dim_hidden) for i in range(0,self.num_classes)]


    def forward(self, hidden, y, epoch):
        selected_components=self.stretch_components_list[epoch]
        models =  [GaussianMixture(selected_components, self.dim_hidden) for i in range(0,self.num_classes)]
        gaussian_center=[]

        class_instances = [hidden[(y == i)] for i in range(0, self.num_classes)]
        for i in range(0, self.num_classes):
            models[i].fit(class_instances[i], n_iter=200)
            gaussian_center.append(models[i].mu.reshape(selected_components, self.dim_hidden))

        gaussian_center_stacked=torch.stack(gaussian_center)

        intra, ap = 0., 0.
        for i in range(0, len(class_instances)):
                # num_list=torch.tensor([w for w in range(len(class_instances)) if w!=i]).cuda()
                # z_list=[]
                # # y_extended = torch.cat(class_instances[i].shape[0] * [self.centers])
                # # x_extended = torch.repeat_interleave(class_instances[i], repeats=self.centers.shape[0], dim=0)
                # # dist = torch.sum((x_extended - y_extended) ** 2, dim=1)
                # # dist= dist.reshape(self.centers.shape[0],class_instances[i].shape[0])
                # # knn = dist.topk(1, largest=False, dim=0)
                # for j in range(0,len(class_instances)):
                #     z,_=torch.max(self.models[j].predict(class_instances[i],probs=True),1)
                #     #z =self.models[j].predict(class_instances[i],probs=True)
                #     z=z.cuda()
                #     z_list.append(z)
                #
                #
                # z_stack=torch.stack(z_list[:])
                #
                # den=torch.sum(torch.index_select(z_stack,0,torch.tensor(i).cuda()))
                # den[torch.isnan(den)] = torch.tensor([0.]).cuda()
                #
                # num=torch.sum(torch.index_select(z_stack, 0, num_list))
                # num[torch.isnan(num)] = torch.tensor([0.]).cuda()



                #ap+=(num+torch.tensor([1e-10]).cuda())/(den +torch.tensor([1e-10]).cuda())
                if len(class_instances[i]) > 0:
                    c = pairwise_distances(class_instances[i], gaussian_center_stacked[i])
                    intra += c

        loss = (self.lambda_c/hidden.shape[0]) * intra
        return loss
