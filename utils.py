import errno
import os
import os.path as osp
import shutil
import sys

import numpy as np

import Albumentations_loader


def centeroidnp(arr):
    length = arr.shape[0]
    return np.sum(arr, axis=0)/length

def std_2d(a):
    x_mean=centeroidnp(a)
    var=np.sum(np.sum((np.subtract(a,x_mean)**2),axis=0))/(a.shape[0])
    return np.sqrt(var)

def mkdir_if_missing(directory):
    if not osp.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise


class AverageMeter(object):
    """Computes and stores the average and current value.

       Code imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def save_checkpoint(state, is_best, fpath='checkpoint.pth.tar'):
    mkdir_if_missing(osp.dirname(fpath))
    torch.save(state, fpath)
    if is_best:
        shutil.copy(fpath, osp.join(osp.dirname(fpath), 'best_model.pth.tar'))


class Logger(object):
    def __init__(self, fpath=None):
        self.file = None
        self.terminal = sys.stdout
        if fpath is not None:
            mkdir_if_missing(os.path.dirname(fpath))
            self.file = open(fpath, 'w')

    def write(self, msg):
        self.terminal.write(msg)
        if self.file is not None:
            self.file.write(msg)

    def flush(self):
        if self.file is not None:
            self.file.flush()
            os.fsync(self.file.fileno())

    def close(self):
        if self.file is not None:
            self.file.close()


def _one_pass(iters):
    for it in iters:
        try:
            yield next(it)
        except StopIteration:
            pass  # of some of them are already exhausted then ignore it.


def zip_varlen(*iterables):
    iters = [iter(it) for it in iterables]
    while True:  # broken when an empty tuple is given by _one_pass
        val = tuple(_one_pass(iters))
        if val:
            yield val
        else:
            break


def compute_intravariance(which,data,labels, num_classes, file_output):
    tmp=which+" Intravariance: "
    print(tmp, file=file_output)
    class_instances = [data[(labels == i)] for i in range(0, num_classes)]
    for idx, i in enumerate(class_instances):
        print("Class ",idx, ": ", len(i), "Std: ", std_2d(i), file=file_output)


import torch

is_torchvision_installed = True
try:
    import torchvision
except:
    is_torchvision_installed = False
import torch.utils.data
import random


class BalancedBatchSampler(torch.utils.data.sampler.Sampler):
    def __init__(self, dataset, train_idx, labels=None):
        self.labels = labels
        self.dataset = dict()
        self.balanced_max = 0
        # Save all the indices for all the classes
        #for idx in range(0, len(dataset)):
        for idx in train_idx:
            label = self._get_label(dataset, idx)
            if label not in self.dataset:
                self.dataset[label] = list()
            self.dataset[label].append(idx)
            self.balanced_max = len(self.dataset[label]) if len(self.dataset[label]) > self.balanced_max else self.balanced_max

        # Oversample the classes with fewer elements than the max
        for label in self.dataset:
            while len(self.dataset[label]) < self.balanced_max:
                self.dataset[label].append(random.choice(self.dataset[label]))
        self.keys = list(self.dataset.keys())
        self.currentkey = 0
        self.indices = [-1] * len(self.keys)

    def __iter__(self):
        while self.indices[self.currentkey] < self.balanced_max - 1:
            self.indices[self.currentkey] += 1
            yield self.dataset[self.keys[self.currentkey]][self.indices[self.currentkey]]
            self.currentkey = (self.currentkey + 1) % len(self.keys)
        self.indices = [-1] * len(self.keys)

    def _get_label(self, dataset, idx, labels=None):
        if self.labels is not None:
            return self.labels[idx].item()
        else:
            # Trying guessing
            dataset_type = type(dataset)
            if is_torchvision_installed and dataset_type is torchvision.datasets.MNIST:
                return dataset.train_labels[idx].item()
            elif is_torchvision_installed and dataset_type is torchvision.datasets.ImageFolder:
                return dataset.imgs[idx][1]
            elif dataset_type is Albumentations_loader.AlbuDataLoader:
                return dataset[idx][1]
            else:
                raise Exception("You should pass the tensor of labels to the constructor as second argument")

    def __len__(self):
        return self.balanced_max * len(self.keys)




# class BalancedPartialBatchSampler(torch.utils.data.sampler.Sampler):
#     def __init__(self, dataset, train_idx, labels=None):
#         self.labels = labels
#         self.dataset = dict()
#         self.balanced_max = 0
#         # Save all the indices for all the classes
#         #for idx in range(0, len(dataset)):
#         for idx in train_idx:
#             label = self._get_label(dataset, idx)
#             if label not in self.dataset:
#                 self.dataset[label] = list()
#             self.dataset[label].append(idx)
#             self.balanced_max = len(self.dataset[label]) if len(self.dataset[label]) > self.balanced_max else self.balanced_max
#
#         # Oversample the classes with fewer elements than the max
#         for idx_class,label in enumerate(self.dataset):
#             while len(self.dataset[label]) < self.balanced_max:
#                 self.dataset[label].append(random.choice(self.dataset[label]))
#         self.keys = list(self.dataset.keys())
#         self.currentkey = 0
#         self.indices = [-1] * len(self.keys)
#
#     def __iter__(self):
#         while self.indices[self.currentkey] < self.balanced_max - 1:
#             self.indices[self.currentkey] += 1
#             yield self.dataset[self.keys[self.currentkey]][self.indices[self.currentkey]]
#             self.currentkey = (self.currentkey + 1) % len(self.keys)
#         self.indices = [-1] * len(self.keys)
#
#     def _get_label(self, dataset, idx, labels=None):
#         if self.labels is not None:
#             return self.labels[idx].item()
#         else:
#             # Trying guessing
#             dataset_type = type(dataset)
#             if is_torchvision_installed and dataset_type is torchvision.datasets.MNIST:
#                 return dataset.train_labels[idx].item()
#             elif is_torchvision_installed and dataset_type is torchvision.datasets.ImageFolder:
#                 return dataset.imgs[idx][1]
#             elif dataset_type is Albumentations_loader.AlbuDataLoader:
#                 return dataset[idx][1]
#             else:
#                 raise Exception("You should pass the tensor of labels to the constructor as second argument")
#
#     def __len__(self):
#         return self.balanced_max * len(self.keys)