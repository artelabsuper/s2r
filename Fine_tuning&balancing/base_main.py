import datetime
import os
import os.path as osp
import random
import time
import matplotlib
from matplotlib.lines import Line2D
#import seaborn as sns
import utils
from GMC_loss import GMC_loss
#from ConvNet import ConvNet
#from LeNet import LeNet
import sigma2_R
from Resnet_original import ResNet18, ResNet50
from utils import AverageMeter, compute_intravariance
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import torch
import torch.nn as nn
from torch.optim import lr_scheduler
from center_loss import CenterLoss



def main(args, trainloader, testloader, num_classes, file_output):
        #fixed colours
        all_colours = num_classes
        colours = .6 * np.random.rand(all_colours, 3) + 0.4
        best_acc=0.

        # if args.pretrained == 'True':
        #     import torchvision.models as models
        #     model = ResNet18(num_classes, pretrained=True)#models.resnet18(pretrained=True)
        #     if model:
        #         print("Model loaded")
        #         #model.fc = nn.Linear(512, num_classes) #Initialize last layer
        #         #model.fc.weight.data = torch.zeros(model.fc.weight.data.shape) #Initialize last layer to zero values
        #         dim_features = model.fc.in_features
        #     else:
        #         print("Loading models error")
        #         exit()
        #
        # else:
        #     print("No one pretrained model loaded")
        #     model = ResNet18(num_classes=num_classes, pretrained=False)
        #     dim_features = model.fc.in_features

        str_model = args.model
        dataset_name = args.path.split('/')[-1]
        if args.pretrained == 'True':
            if dataset_name == 'CUB_200_2011':
                if str_model =='resnet18':
                    model = ResNet18(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512, num_classes)
                elif str_model =='resnet50':
                    model = ResNet50(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512 * 4, num_classes)
                else:
                    print("Error chosen model")
                    exit()
                if model:
                    print("Model loaded")
                    dim_features = model.fc.in_features
                else:
                    print("Loading models error")
                    exit()
            elif dataset_name == 'cifar100':
                if str_model =='resnet18':
                    model = ResNet18(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512, num_classes)
                elif str_model =='resnet50':
                    model = ResNet50(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512 * 4, num_classes)
                else:
                    print("Error chosen model")
                    exit()
                if model:
                    print("Model loaded")
                    dim_features = model.fc.in_features
                else:
                    print("Loading models error")
                    exit()
            elif dataset_name == 'fgvc_dataset':
                if str_model == 'resnet18':
                    model = ResNet18(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512, num_classes)
                elif str_model == 'resnet50':
                    model = ResNet50(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512 * 4, num_classes)
                else:
                    print("Error chosen model")
                    exit()
                if model:
                    print("Model loaded")
                    dim_features = model.fc.in_features
                    print(dim_features)
                else:
                    print("Loading models error")
                    exit()
            elif dataset_name == 'Imagenet-ILSVRC2012':
                if str_model == 'resnet18':
                    model = ResNet18(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512, num_classes)
                elif str_model == 'resnet50':
                    model = ResNet50(num_classes=1000, pretrained=True)
                    model.fc = nn.Linear(512 * 4, num_classes)
                else:
                    print("Error chosen model")
                    exit()
                if model:
                    print("Model loaded")
                    dim_features = model.fc.in_features
                    #print(dim_features)
                else:
                    print("Loading models error")
                    exit()
            else:
                print("Error select dataset, please modify the path parameter")
                print("Dataset name extracted: ", dataset_name)
                exit()

        else:
            print("Set pretrained=True, it's better.. exit()")
            exit()

        print("We have available ", torch.cuda.device_count(), "GPUs!")
        gpus_ids = [i for i in range(0, torch.cuda.device_count())]
        model = nn.DataParallel(model, device_ids=gpus_ids).cuda()

        criterion_xent = nn.CrossEntropyLoss()
        criterion_loss=None

        if args.loss == 'center_loss':
            print("CenterLoss set")
            print("Features dimension: ", dim_features)
            criterion_loss = CenterLoss(dim_hidden=dim_features, num_classes=num_classes, lambda_c=args.lambda_c)


        if args.loss == 'sigma2_R':
            print("sigma2_R set")
            print("Features dimension: ", dim_features)
            criterion_loss = sigma2_R.s2r_loss(args, dim_hidden=dim_features, num_classes=num_classes)


        if args.loss=='sigma2_R' or args.loss=='center_loss':
            optimizer = torch.optim.SGD(model.parameters(), weight_decay=1e-4, lr=args.lr, momentum=0.9)
            optimizer_c = torch.optim.SGD(criterion_loss.parameters(), weight_decay=1e-4, lr=args.second_lr, momentum=0.9)
        elif args.loss =='xent_loss':
            optimizer = torch.optim.SGD(model.parameters(), weight_decay=1e-4, lr=args.lr, momentum=0.9)
            optimizer_c = None


        scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch)
        #scheduler=torch.optim.lr_scheduler.StepLR(optimizer, step_size=30, gamma=0.1)


        if args.loss == 'center_loss' or args.loss == 'sigma2_R':
            second_scheduler = lr_scheduler.CosineAnnealingLR(optimizer_c, args.max_epoch)

        start_time = time.time()

        #Test pretrained
        # print("==> Test", file=file_output)
        # acc, err = test(model, testloader, None, None, args, file_output, num_classes, 0)
        # print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err), file=file_output)
        # exit()
        #########################

        for epoch in range(args.max_epoch):

            #lr=scheduler.get_lr()[0]

            #if args.loss != 'xent_loss':
                #lr_cent = scheduler_cent.get_lr()[0]
                #print("LR: ", lr, "LR cent: ", lr_cent)
            # if args.loss == 'sigma2_R':
            #     print("K_normalizer: ", criterion_loss.K_normalizer.data.cpu().numpy().transpose(), file=file_output)

            train_features, train_labels=train(trainloader, model, criterion_xent, criterion_loss,optimizer,optimizer_c, num_classes, epoch, args, file_output,colours)
            print("==> Epoch {}/{}".format(epoch + 1, args.max_epoch)," Lr: ", utils.get_lr(optimizer), file=file_output)
            print("==> Test", file=file_output)
            acc, err = test(model, testloader, train_features, train_labels, args, file_output,num_classes, epoch)
            print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err), file=file_output)
            scheduler.step()
            if args.loss =='center_loss' or args.loss =='sigma2_R':
                second_scheduler.step()
            if args.save_model and acc > best_acc:
                saved_model_path=osp.join(args.save_dir, '_'+ dataset_name + '_'+'weights_model_'+args.model+'loss_'+ args.loss)
                torch.save(model.state_dict(), saved_model_path)
                print("Model saved")
                best_acc=acc

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed), file=file_output)
        return True


def train(trainloader, model, criterion_xent, criterion_loss,
          optimizer,optimizer_c, num_classes, epoch, args, file_output, colours):

    model.train()
    xent_losses = AverageMeter()
    GMC_losses = AverageMeter()
    sigma2_R_losses= AverageMeter()
    center_losses = AverageMeter()
    all_features, all_labels, all_paths = [], [], []
    for batch_idx, (data, labels) in enumerate(trainloader):
            data, labels = data.cuda(), labels.cuda()
            outputs, features = model(data)
            loss_xent = criterion_xent(outputs, labels)
            if args.loss == 'sigma2_R':
                sigma2_R = criterion_loss(features, labels)
                loss = sigma2_R + loss_xent

            if args.loss == 'xent_loss':
                loss = loss_xent

            elif args.loss == 'center_loss':
                center_loss = criterion_loss(features, labels)
                loss = loss_xent + center_loss

            optimizer.zero_grad()

            # if args.loss != 'xent_loss'  and  args.loss!='CGM_loss':
            #     optimizer_c.zero_grad()

            #TODO to detach parameter from learning step (not computed during gradient descendent)
            # if args.graft <= epoch:
            #     criterion_loss.centers.detach_()

            loss.backward()
            optimizer.step()

            if args.loss=='sigma2_R' or args.loss=='center_loss':
                optimizer_c.step()

            xent_losses.update(loss_xent.item(), labels.size(0))
            all_features.append(features.data.cpu().numpy())
            all_labels.append(labels.data.cpu().numpy())


            if args.loss=='sigma2_R':
                    sigma2_R_losses.update(sigma2_R.item(), labels.size(0))
                    if (batch_idx + 1) % 10== 0:
                        print("Batch {}\t CrossEntropy {:.6f} ({:.6f}) sigma2_R {:.6f} ({:.6f})".format(batch_idx + 1,
                                                                                                       xent_losses.val,
                                                                                                       xent_losses.avg,
                                                                                                       sigma2_R_losses.val,
                                                                                                       sigma2_R_losses.avg),
                                                       file=file_output)

            if args.loss == 'center_loss':
                center_losses.update(center_loss.item(), labels.size(0))
                if (batch_idx + 1) % 10 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f}) CenterLoss {:.6f} ({:.6f})".format(batch_idx + 1,
                                                                                                      xent_losses.val,
                                                                                                      xent_losses.avg,
                                                                                                      center_losses.val,
                                                                                                      center_losses.avg),
                          file=file_output)

            if args.loss == 'xent_loss':
                if (batch_idx + 1) % 10 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1,xent_losses.val,xent_losses.avg),file=file_output)

    all_features = np.concatenate(all_features, 0)
    all_labels = np.concatenate(all_labels, 0)

    # if (epoch + 1) % 1 == 0:
    #     #gcm = utils.Gaussian_cm(torch.cat(all_features), torch.cat(all_labels), num_classes)
    #     #utils.computer_covariance("Train", all_features, all_labels, num_classes, file_output)
    #     #voronoi_metric(all_features, all_labels, num_classes, file_output)
    #     #compute_intravariance("Train", torch.cat(all_features), torch.cat(all_labels), num_classes, file_output)
    #     #plot_features(all_features, all_labels, num_classes, epoch, args, all_paths,criterion_loss,colours)
    #     pass

    return all_features, all_labels

def test(model, testloader, train_features, train_labels, args, file_output, num_classes, epoch):
    model.eval()
    all_features, all_labels=[],[]
    correct, total = 0, 0
    #confusion_matrix = np.zeros((num_classes, num_classes), dtype=int)
    with torch.no_grad():
        for idx, (data, labels) in enumerate(testloader):
                data, labels = data.cuda(), labels.cuda()
                outputs, features = model(data)

                #all_features.append(features.data.cpu().numpy())
                #all_labels.append(labels.data.cpu().numpy())

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().float().item()

                # for t, p in zip(predicted, labels):
                #     confusion_matrix[int(t), int(p)] += 1


    #test_features = np.concatenate(all_features, 0)
    #test_labels = np.concatenate(all_labels, 0)
    #compute_intravariance("Test", torch.cat(test_features), torch.cat(test_labels), num_classes, file_output)
    #utils.compute_precision(confusion_matrix, file_output)
    # if (epoch + 1) % 99 == 0:
    #     density_plot(train_features, train_labels, test_features, test_labels, num_classes, epoch, args)
    acc = correct * 100. / total
    err = 100. - acc
    return acc, err


# def density_plot(train_features, train_labels, test_features, test_labels, num_classes, epoch, args):
#     dirname = osp.join(args.save_dir, 'Density_plot_gpu_' + args.gpu + args.path.split('/')[-2] + '_' + args.loss)
#     if not osp.exists(dirname):
#         os.mkdir(dirname)
#
#     class_train_instances = [train_features[(train_labels == i)] for i in range(0, num_classes)]
#
#     class_test_instances = [test_features[(test_labels == i)] for i in range(0, num_classes)]
#
#
#     for i in range(len(class_train_instances)):
#         sns.kdeplot(class_train_instances[i][:, 0], class_train_instances[i][:, 1], cmap="YlOrRd",  # YlGnBu
#                     shade=True,
#                     shade_lowest=True,
#                     n_levels=40,
#                     antialiased=True)
#         sns.scatterplot(class_test_instances[i][:, 0], class_test_instances[i][:, 1], marker="x", color="b", alpha=0.3, size=1)
#         save_name = osp.join(dirname, 'epoch_' + str(epoch + 1) + " Class: " + str(i) + '.png')
#         custom = [Line2D([], [], marker='.', color='r', linestyle='None'),
#                   Line2D([], [], marker='.', color='b', linestyle='None')]
#         plt.legend(custom, ['Train Feat', 'Test Feat'], loc='upper right')
#         plt.savefig(save_name, bbox_inches='tight', dpi=400)
#         plt.close()
#
#
# def plot_features(features, labels, num_classes, epoch, args, all_paths, criterion_loss, colours):
#     #centers=criterion_loss.centers.data.cpu().numpy()
#     #colors=['R','G','B']
#     plt.tick_params(labelsize=14)
#     #splitted=[i[0].split('_') for i in all_paths]
#     #class_color=[i[1][-1] for i in splitted]
#     #idx_instance = [i[2] for i in splitted]
#     #noise_color = [i[3] for i in splitted]
#
#     # tsne = TSNE(n_components=2, init='pca', n_iter=250)
#     # tSNE_result = tsne.fit_transform(features)
#     # tSNE_label = labels
#     # print(tSNE_result)
#     # exit()
#     for label_idx in range(num_classes):
#         plt.scatter(
#             features[labels == label_idx, 0],
#             features[labels == label_idx, 1],
#             facecolor=colours[label_idx],
#             #marker=noise_marker,
#             s=2,
#         )
#
#     # for feat,l,cl_color,noise in zip(features,labels,class_color,noise_color):
#     #     if noise=='N':
#     #         plt.plot(feat[0],feat[1],c=colors[l], marker='o', markersize=1)
#     #     else:
#     #         plt.plot(feat[0], feat[1], c=colors[l], marker='x', markersize=10)
#
#     #plt.scatter(centers[:,0],centers[:,1], marker='x', s=5, facecolor='black')
#
#     # plt.legend(loc='upper right')
#     dirname = osp.join(args.save_dir, 'plot_gpu_'+args.gpu + args.path.split('/')[-2] + '_'+args.loss)
#     if not osp.exists(dirname):
#         os.mkdir(dirname)
#     save_name = osp.join(dirname, 'epoch_' + str(epoch + 1) + '.png')
#     plt.savefig(save_name, bbox_inches='tight', dpi=400)
#     plt.close()
