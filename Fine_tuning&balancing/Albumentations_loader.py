from os.path import basename, join, splitext
import glob
import cv2
import numpy as np
import torch
from torch.utils import data
import matplotlib.pyplot as plt
import albumentations as albu
from albumentations.pytorch.transforms import ToTensorV2
import os

class AlbuDataLoader(data.Dataset):
    def __init__(self, folder_path, img_in_size, img_out_size,all_labels,augmentation=None, as_tensor=True):
        super(AlbuDataLoader, self).__init__()
        self.img_in_size = img_in_size
        self.img_out_size = img_out_size
        # if name is None:
        #     self.name = basename(folder_path)
        # else:
        #     self.name = name
        self.augmentation = augmentation
        self.as_tensor = as_tensor
        self.all_folders=[i for i in os.listdir(folder_path)]
        self.labels=all_labels
        self.img_files = glob.glob(folder_path+'/*/*.png')


    def __getitem__(self, index):
        # read data using cv2
        label = self.labels[index]
        img_path = self.img_files[index]
        image = cv2.imread(img_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image)
            image = sample['image'].float()
            #image=image.permute(2,1,0)

        # # apply preprocessing
        # if self.as_tensor:
        #     image = cv2.resize(image, (self.img_in_size, self.img_in_size), interpolation=cv2.INTER_AREA)
        #     image = image_to_tensor(image)
        return image,label
    def __len__(self):
        return len(self.img_files)

# def image_to_tensor(cv2_image):
#     if len(cv2_image.shape) > 2:
#         cv2_image = np.transpose(cv2_image, (2, 0, 1))  # color image
#     cv2_image = cv2_image.astype(np.float32)
#     image_tensor = torch.tensor(cv2_image, dtype=torch.float)
#     image_tensor = image_tensor / 255.
#     return image_tensor

# helper function for data visualization
def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(12, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title())
        plt.imshow(image)
    plt.show()