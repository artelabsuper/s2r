import numpy as np

def kl_divergence(p, q):
    return np.sum(np.where(p != 0, p * np.log(p / q), 0))


p=np.array([0.9, 0.05, 0.05])
q=np.array([0.8, 0.1, 0.1])
print(kl_divergence(p,q))
