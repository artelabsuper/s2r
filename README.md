# σ²-R Loss: A weighted loss by multiplicative factors using sigmoidal functions
We introduce a new loss function called sigma squared reduction loss (σ²-R Loss), which is regulated by a sigmoid function to inflate/deflate the error per instance and then continue to reduce the class intra-variance.
Our loss has clear intuition and geometric interpretation, furthermore, we demonstrate by experiments the effectiveness of our proposal on several benchmark datasets showing the intra-variance reduction and overcoming the results obtained with center loss and soft nearest neighbour functions.

You can find all the implementation details in this repository.

## Usage
* Install requirements
```
pip install -r requirements.txt
```
* download [cifar10](https://www.kaggle.com/c/cifar-10/data), [cifar100](https://www.kaggle.com/pavansanagapati/cifar100) and [FashionMNIST](https://github.com/zalandoresearch/fashion-mnist) dataset (the dataset folder should have a sub-folder for each class)
* Execute 10 runs of the same experiment.
```
# this command run 10 experiments using the specificed loss (xent and center loss are also availables for comparison)
python3.7 N_run.py --path /dataset/path/ --loss [sigma2_R|xent_loss|center_loss] --lambda0 0.01 --max-epoch 300 --lr_cent 0.1 --lr 0.001 --gpu 0 --batch-size 512 --knn 7 --dataset eggsmentations
```
## Update
In the Fine-Tuning folder we use a Fine-Tuning technique and the new balancing data loader logic on CUB-200-2011 dataset and resnet18 pretrained model.
```
# this command run experiment using specified loss
--path dataset_path --loss sigma2_R --lambda_c 0.0001 --max-epoch 100  --lr 0.001 --second_lr 0.01 --gpu X --save_model True --batch-size 40 --model resnet18 --balanced_classes 10 --pretrained True --dataset MyDataset_plus_annotation
```
You need to run N_run.py file inside the Fine-Tuning folder.
# Graphical Result

## Feature space representation
Quality comparison on the training set, between the σ²-R loss function and two other loss functions on the Fuzzy-RGB dataset we created.
In (a) the 2D representation of the deep features found by a LeNet with PReLU activation function that uses the cross-entropy function, in (b) the same network trained with the center loss function and in (c) the deep features arranged around the class centroids thanks at the work of our loss function. To view the deep features, CNN's penultimate layer uses only two neurons. Figures (b) and (c) show similar behaviour but in (c) the cluster around the class centroid is much more compact.
![](images/graphical_representation.jpg) 

## Proposed loss fuction surface description

Surface built by our loss (red-yellow) than Center Loss (blue plane)            | Delta sigmoid's growth variable          | Delta inflection point (over epochs)
:-------------------------:|:-------------------------:|:-------------------------:
![](images/loss_surface1.gif)  |  ![](images/loss_surface.gif) | ![](images/delta_inflection.gif)


# Quantitative results

## Accuracy
In this table we can see the max accuracy of the proposed approach compared with classical crossentropy and centerloss.

|  Dataset | σ²-R|baseline|base+centerloss|soft N|
|----------|-----------|-----------|-----------|-----------|
|Cifar 10|**92.01**|90.78|91.72|91.22|
|Cifar 100|**62.65**|58.18|61.57|-|


## Intra variance
In the following table we can see the intra-variance results on FashionMNIST dataset, δ % is the percentage difference between intra-variances.

![](images/intravariance_table.jpg) 


## Citation
More details and results in published work:

```
@article{grassa2020dynamic,
    title={σ²-R Loss: A weighted loss by multiplicative factors using sigmoidal functions},
    author={Riccardo La Grassa and Ignazio Gallo and Nicola Landro},
    year={2020},
    eprint={},
    archivePrefix={arXiv},
    primaryClass={cs.LG}
}
```
