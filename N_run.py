import os
import os.path as osp
import torch
from torch.backends import cudnn
import base_main
import argparse
import datasets_semplified
from utils import Logger


def args_parser():
    parser = argparse.ArgumentParser("Center Loss Example")
    parser.add_argument('-d', '--dataset', type=str, default='MyDataset')
    parser.add_argument('-j', '--workers', default=4, type=int,
                        help="number of data loading workers (default: 4)")
    parser.add_argument('--batch-size', type=int, default=128)
    parser.add_argument('--lr', type=float, default=0.01, help="learning rate for model")
    parser.add_argument('--lr_cent', type=float, default=0.01, help="learning rate for center")
    parser.add_argument('--max-epoch', type=int, default=500)
    parser.add_argument('--stepsize', type=int, default=50)
    parser.add_argument('--gamma', type=float, default=0.5, help="learning rate decay")
    parser.add_argument('--alpha', type=float, default=0.5, help='learning rate of class center (default: 0.5)')
    parser.add_argument('--eval-freq', type=int, default=1)
    parser.add_argument('--percentage_used', type=float, default=1.)
    parser.add_argument('--print-freq', type=int, default=50)
    parser.add_argument('--gpu', type=str, default='0')
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--save-dir', type=str, default='log')
    parser.add_argument('--path', type=str, default='dataset')
    parser.add_argument('--graft', type=int, default=5)
    parser.add_argument('--when_save_model', type=int, default='100', help="save model at epoch x")
    parser.add_argument('--plot', action='store_true', help="whether to plot features for every epoch")
    parser.add_argument('--k_normalizer', type=int, default=5)
    parser.add_argument('--knn', type=int, default=3)
    parser.add_argument('--range_f', type=int, default=1)
    parser.add_argument('--loss', type=str)
    parser.add_argument('--pretrained', type=str, default='False', help="load pretrained model")
    parser.add_argument('--lambda0', type=float, default=1.)
    return parser.parse_args()


n_exp=1
args=args_parser()
# k_normalizer=[i for i in range(2,30,2)]
# range_f =[i for i in range(1,5,1)]
torch.manual_seed(args.seed)
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
use_gpu = torch.cuda.is_available()
print("Let's use", torch.cuda.device_count(), "GPUs!")
str_dataset=args.path.split('/')[-2]
#print("Creating dataset: {}".format(args.dataset))
if args.dataset=='MyDataset_cross_val':
    print("Cross-validation set..")
    kf = 10
    for idx_k in range(kf):
        dataset = datasets_semplified.create(
            name=args.dataset, batch_size=args.batch_size, use_gpu=use_gpu,
            num_workers=args.workers, percentage_used=args.percentage_used, path=args.path
        )
        trainloader, testloader, num_classes = dataset.trainloader, dataset.testloader, dataset.num_classes
        if args.loss == 'sigma2_R':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu  + '_knn=' + str(args.knn) + '_loss=' + args.loss+ '_lambda='+ str(args.lambda0)+'_lr_cent='+str(args.lr_cent)))
        elif args.loss == 'xent_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu  + '_knn=' + '_loss=' + args.loss))
        elif args.loss == 'center_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss + '_lambda=' + str(args.lambda0)+'_lr_cent='+str(args.lr_cent)))
        if use_gpu:
            print("Lr {:}  Batch-size {:}  Loss = {:} Graft = {:} Lambda = {:}".format(args.lr, args.batch_size, args.loss, args.graft, args.lambda0),file=file_output)
            cudnn.benchmark = True
            torch.cuda.manual_seed_all(args.seed)
        else:
            print("Currently using CPU, abort()", file=file_output)
            exit()
        if base_main.main(args, trainloader, testloader, num_classes, file_output):
            print("Success", file=file_output)
            file_output.close()
        else:
            print("Abort", file=file_output)
            exit()
#
else:
    print("Cross-validation set..")
    kf = 10
    for idx_k in range(kf):
        dataset = datasets_semplified.create(
            name=args.dataset, batch_size=args.batch_size, use_gpu=use_gpu,
            num_workers=args.workers, percentage_used=args.percentage_used, path=args.path
        )
        trainloader, testloader, num_classes = dataset.trainloader, dataset.testloader, dataset.num_classes
        print("K-FOLD: ", idx_k)
        if args.loss == 'sigma2_R':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu  + '_knn=' + str(args.knn) + '_loss=' + args.loss+ '_lambda='+ str(args.lambda0)+'_lr_cent='+str(args.lr_cent)))
        elif args.loss == 'xent_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu  + '_knn=' + '_loss=' + args.loss))
        elif args.loss == 'center_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss + '_lambda=' + str(args.lambda0)+'_lr_cent='+str(args.lr_cent)))
        if use_gpu:
            print("Lr {:}  Batch-size {:}  Loss = {:} Lambda = {:}".format(args.lr,args.batch_size,args.loss,args.lambda0),file=file_output)
            cudnn.benchmark = True
            torch.cuda.manual_seed_all(args.seed)
        else:
            print("Currently using CPU, abort()", file=file_output)
            exit()
        if base_main.main(args, trainloader, testloader, num_classes, file_output):
            print("Success", file=file_output)
            file_output.close()
        else:
            print("Abort", file=file_output)
            exit()