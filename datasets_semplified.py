import albumentations as albu
import numpy
import numpy as np
import torch
import torchvision
from torch.utils.data.sampler import SubsetRandomSampler
from torchvision import datasets
from torchvision.datasets import ImageFolder

# from scipy.ndimage import imread
# from class_select import DatasetMaker, get_class_i
# from Albumentations_loader import AlbuDataLoader
from utils import BalancedBatchSampler


def get_training_augmentation():
    train_transform = [
     #albu.IAAAdditiveGaussianNoise(p=0.5),
     #albu.IAAAdditiveGaussianNoise(p=0.5),
     albu.ShiftScaleRotate(p=0.5),
     #albu.IAAPerspective(p=0.5),
    albu.OneOf([
        albu.HorizontalFlip(),
        albu.VerticalFlip(),
    ], p=1),

     #albu.HorizontalFlip(p=0.5), #ONEOF TODO!!
     albu.Rotate(p=0.5),
     albu.RandomResizedCrop(28,28, scale=(0.7, 1.0),p=0.5),
     #albu.VerticalFlip(p=0.5), #ONEOF TODO!!
     #albu.ChannelShuffle(p=0.5),
     #albu.RandomGamma(p=0.5),
     #albu.RandomGridShuffle(grid=(3, 3), p=0.5),
     #albu.RandomRotate90(p=0.5),
    ]
    return albu.Compose(train_transform,p=0.5)


# def random_pick(t_list):
#     np.random.seed(0)
#     final_t_list=[]
#     for i in range(len(t_list)):
#         randomness_flag = bool(random.getrandbits(1))
#         if randomness_flag:
#             final_t_list.append(t_list[i])
#
#     return final_t_list


class MyDataset(object):
    def __init__(self, batch_size, use_gpu, num_workers, percentage_used, path):
        trainloader=[]
        train_labels=[]
        testloader=[]
        test_labels=[]
        self.batch_size = batch_size
        self.percentage_used = percentage_used
        self.path=path

        transform = [torchvision.transforms.RandomRotation(30), torchvision.transforms.RandomHorizontalFlip(),
                     torchvision.transforms.RandomVerticalFlip(),
                     #torchvision.transforms.ColorJitter(brightness=(0, 50), contrast=(0, 30), saturation=(0, 40),hue=(-0.5, 0.5)),
                     torchvision.transforms.RandomResizedCrop(size=(28,28), scale=(0.9, 1.0))]

        train_transforms = torchvision.transforms.Compose([
            #torchvision.transforms.Resize(size=(32, 32)),
            #torchvision.transforms.RandomApply(random_pick(transform), p=0.5),
            torchvision.transforms.ToTensor(),
        ])

        test_transforms = torchvision.transforms.Compose([
            #torchvision.transforms.Resize(size=(32, 32)),
            torchvision.transforms.ToTensor(),
        ])


        train_dataset = datasets.ImageFolder(path+'/train', transform=train_transforms)
        train_idx=[i for i in range(len(train_dataset.samples))]
        train_loader = torch.utils.data.DataLoader(train_dataset, sampler=BalancedBatchSampler(train_dataset, train_idx), batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True) #for each batch we have batch-size/class instances.

        test_dataset = datasets.ImageFolder(path + '/test', transform=test_transforms)
        test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)

        self.trainloader = train_loader
        self.testloader = test_loader
        self.num_classes = len(train_dataset.classes)
        print("Full Dataset size: ", len(train_dataset.samples), " Dataset percentage used: ", percentage_used * 100, "% ","Classes: ", self.num_classes, "Test loader set: ", len(test_dataset.samples))


class MyDataset_cross_val(object):
    def __init__(self, batch_size, use_gpu, num_workers, percentage_used, path):
        self.batch_size = batch_size
        self.percentage_used = percentage_used
        self.path=path

        train_transforms = torchvision.transforms.Compose([
            torchvision.transforms.Resize((32,32)),
            ToNumpy(),
            AlbumentationToTorchvision(get_training_augmentation()),
            torchvision.transforms.ToTensor()
        ])

        # test_transforms = torchvision.transforms.Compose([
        #     torchvision.transforms.ToTensor(),
        # ])




        train_dataset = ImageFolder(path, transform=train_transforms)
        num_train = len(train_dataset)
        indices = list(range(num_train))
        split = int(np.floor(0.2 * num_train))

        np.random.seed(np.random.randint(0,1000))
        np.random.shuffle(indices)
        train_idx, valid_idx = indices[split:], indices[:split]
        train_sampler = SubsetRandomSampler(train_idx)
        test_sampler = SubsetRandomSampler(valid_idx)


        train_loader = torch.utils.data.DataLoader(train_dataset, sampler=BalancedBatchSampler(train_dataset, train_idx), batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True) #for each batch we have batch-size/class instances.
        test_loader = torch.utils.data.DataLoader(train_dataset, sampler=test_sampler, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)
        self.trainloader = train_loader
        self.testloader = test_loader
        self.num_classes = len(train_dataset.classes)
        print("Full Dataset size: ", len(train_dataset), " Train size: ",len(train_loader.sampler),
              "Classes: ", self.num_classes, "Test loader set: ", len(test_loader.sampler))


class eggsmentations(object):
    def __init__(self, batch_size, use_gpu, num_workers, percentage_used, path):
        self.batch_size = batch_size
        self.percentage_used = percentage_used
        self.path=path

        train_transforms = torchvision.transforms.Compose([
            ToNumpy(),
            AlbumentationToTorchvision(get_training_augmentation()),
            torchvision.transforms.ToTensor()
        ])

        test_transforms = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
        ])


        train_dataset = datasets.ImageFolder(path + '/train', transform=train_transforms)

        train_idx = [i for i in range(len(train_dataset))]

        train_loader = torch.utils.data.DataLoader(train_dataset,sampler=BalancedBatchSampler(train_dataset, train_idx),batch_size=batch_size, shuffle=False, num_workers=16,
                                                   pin_memory=True)
        test_dataset = datasets.ImageFolder(self.path + '/test', transform=test_transforms)
        test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=16, pin_memory=True)

        self.trainloader = train_loader
        self.testloader = test_loader
        self.num_classes = len(train_dataset.classes)
        print("Full Dataset size: ", len(train_dataset), " Dataset percentage used: ", percentage_used * 100, "% ","Classes: ", self.num_classes, "Test loader set: ", len(test_dataset.samples))




class AlbumentationToTorchvision:
    def __init__(self, compose):
        self.compose = compose

    def __call__(self, x):
        data = {"image": x}
        return self.compose(**data)['image']


class ToNumpy:
    def __call__(self, x):
        return numpy.array(x)




__factory = {
    'MyDataset': MyDataset,
    'MyDataset_cross_val': MyDataset_cross_val,
    'eggsmentations': eggsmentations
}


def create(name, batch_size, use_gpu,num_workers, percentage_used, path):
    if name not in __factory.keys():
        raise KeyError("Unknown dataset: {}".format(name))
    return __factory[name](batch_size, use_gpu, num_workers,percentage_used, path)
